import os


class LockFile:
    def __init__(self, filename):
        self.filename = None
        if os.path.isfile(filename):
            raise Exception("Script is already running (says interlock file %s)" % filename)
        self.filename = filename
        f = open(self.filename, "w")
        f.writelines(('Interlock file', ))
        f.close()

    def __del__(self):
        if self.filename:
            os.remove(self.filename)
