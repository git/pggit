import requests


class test(object):
    def __init__(self, cfg):
        pass

    def pushtrigger(self, reponame, username):
        print("Firing push trigger for repository '%s', due to push by %s" % (reponame, username))


class varnishpurger(object):
    """
    Push trigger that purges repositories from varnish. The idea being that
    the repositories follow a standard gitweb url structure, and we purge
    all pages related to that repository.

    Requires the config variable "host" set in the section "varnishpurge".
    This can be an IP address (typically 127.0.0.1) or IP address + port
    (typically 127.0.0.1:81). The trigger will always post to the URL
    "/varnish-purge-url", and will include the URL to purge in the form of
    a regular expression in the custom header X-Purge-URL.
    """
    def __init__(self, cfg):
        self.host = cfg.get('varnishpurge', 'host')

    def pushtrigger(self, reponame, username):
        # Make a callback to a local varnish server to purge a repository
        # from it, both gitweb and cgit.
        # Also, purge the actual http serving git repo itself.
        for u in [
                r'^/gitweb/?$',
                r'^/gitweb/\?p=%s.git' % reponame,
                r'^/git/%s' % reponame,
                r'^/cgit/?$',
                r'^/cgit/%s.git' % reponame,
        ]:
            if not self._internal_purge(u):
                print("Varnish purge failed, website may become slightly out of date")
                return

    def _internal_purge(self, url):
        try:
            resp = requests.get(
                "http://{0}/varnish-purge-url".format(self.host),
                headers={
                    'X-Purge-URL': url,
                }
            )
            if resp.status_code == 200:
                return True
            return False
        except Exception as ex:
            return False
