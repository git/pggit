# vim: ai ts=4 sts=4 sw=4

from django.contrib.syndication.views import Feed
from gitadmin.adm.models import *

import datetime


class AllReposFeed(Feed):
    title = "pggit - all repositories"
    link = "/"
    description = "All pggit repositories, including those not approved yet"
    description_template = "feeds/all.tmpl"

    def items(self):
        return Repository.objects.all().order_by('repoid')

    def item_link(self, repo):
        return "https://git.postgresql.org/gitweb/%s" % repo
