# vim: ai ts=4 sts=4 sw=4
import re

from django.http import HttpResponse, HttpResponseRedirect
from django.template import RequestContext
from django.shortcuts import render, get_object_or_404
from django.contrib.auth.decorators import login_required, user_passes_test
from django.forms.models import inlineformset_factory
from django.db import transaction

import datetime

from gitadmin.adm.models import *
from gitadmin.adm.forms import *


# Utility classes
class FormIsNotValid(Exception):
    pass


# Utility functions
def _MissingSshkey(user):
    if not user.is_authenticated:
        return False
    try:
        gu = GitUser.objects.get(user=user)
        if gu.sshkey != '':
            return False
        else:
            return True
    except:
        return True


def context_add(request):
        return {
            'missing_sshkey': _MissingSshkey(request.user),
        }


# Views
@login_required
def index(request):
    repos = Repository.objects.extra(
        where=["remoterepository_id IS NULL AND repoid IN (SELECT repository FROM repository_permissions where user_id=%s)"],
        select={'perm': "SELECT CASE WHEN level>1 THEN 't'::boolean ELSE 'f'::boolean END FROM repository_permissions WHERE user_id=%s AND repository_permissions.repository=repositories.repoid"},
        params=[request.user.id], select_params=[request.user.id]).order_by('name')
    return render(request, 'index.html', {
        'repos': repos,
    })


def help(request):
    return render(request, 'help.html')


@login_required
@transaction.atomic
def editrepo(request, repoid):
    repo = get_object_or_404(Repository, repoid=repoid)
    repo.ValidateOwnerPermissions(request.user)
    form = None

    formfactory = inlineformset_factory(
        Repository,
        RepositoryPermission,
        extra=0,
        fields=['username', 'level'],
        form=RepositoryPermissionForm,
    )

    if request.method == "POST":
        form = RepositoryForm(data=request.POST, instance=repo)
        formset = formfactory(data=request.POST, instance=repo)
        addform = RepositoryPermissionsAddForm(repo=repo, data=request.POST)
        del form.fields['approved']
        if repo.approved:
            del form.fields['initialclone']

        if form.is_valid() and formset.is_valid() and addform.is_valid():
            try:
                # Manually validate the repository entered if there is one to clone
                if 'initialclone' in form.cleaned_data and form.cleaned_data['initialclone']:
                    if form.cleaned_data['initialclone'].startswith('git://'):
                        # Validate hostnames and stuff?
                        pass
                    else:
                        # Assume local
                        try:
                            r = Repository.objects.get(name=form.cleaned_data['initialclone'])
                            if not r.anonymous:
                                form._errors['initialclone'] = form._errors.get('initialclone', [])
                                form._errors['initialclone'].append('Specified repository is not available anonymously')
                                raise FormIsNotValid()
                        except Repository.DoesNotExist:
                            form._errors['initialclone'] = form._errors.get('initialclone', [])
                            form._errors['initialclone'].append('Specified repository does not exist')
                            raise FormIsNotValid()

                form.save()
                formset.save()
                if addform.cleaned_data['addemail']:
                    RepositoryPermission(
                        user=get_or_import_user(addform.cleaned_data['addemail']),
                        repository=repo,
                        level=addform.cleaned_data['addlevel'],
                    ).save()
                return HttpResponseRedirect("../../")
            except FormIsNotValid:
                # Just continue as if the form wasn't valid, expect the caller
                # to have set the required error fields
                pass

    else:
        form = RepositoryForm(instance=repo)
        del form.fields['approved']
        if repo.approved:
            del form.fields['initialclone']
        formset = formfactory(instance=repo)
        addform = RepositoryPermissionsAddForm(repo=repo)

    perm = repo.repositorypermission_set.all()

    return render(request, 'repoview.html', {
        'form': form,
        'formset': formset,
        'addform': addform,
        'repo': repo,
        'repoperm': perm,
    })


@login_required
@transaction.atomic
def deleterepo(request, repoid):
    repo = get_object_or_404(Repository, repoid=repoid)
    repo.ValidateOwnerPermissions(request.user)

    if request.method == 'POST':
        form = ConfirmDeleteForm(data=request.POST)

        if form.is_valid():
            repo.delete()
            return HttpResponseRedirect('../../../')
    else:
        form = ConfirmDeleteForm()

    return render(request, 'deleterepo.html', {
        'form': form,
    })


@login_required
@transaction.atomic
def newrepo(request):
    if request.method != "POST":
        return HttpResponse("Must be posted!")
    newname = request.POST['reponame']
    r = re.compile(r'^[a-z0-9-_/]{5,64}$')
    if not r.match(newname):
        return HttpResponse("Format of project name is invalid!")

    repo = Repository(name=newname)
    repo.save()
    perm = RepositoryPermission(user=request.user, repository=repo, level=2)
    perm.save()

    return HttpResponseRedirect('../repo/%s/' % repo.repoid)
