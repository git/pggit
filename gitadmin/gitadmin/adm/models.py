from django.contrib.auth.models import User
from django.db import models
import datetime

PERMISSION_CHOICES = (
    (0, 'Read'),
    (1, 'Write'),
    (2, 'Owner'),
)


class RemoteRepositoryType(models.Model):
    repotype = models.CharField(max_length=16, blank=False, null=False, primary_key=True)

    def __str__(self):
        return self.repotype

    class Meta:
        db_table = 'remoterepositorytypes'


class RemoteRepository(models.Model):
    repotype = models.ForeignKey(RemoteRepositoryType, null=False, on_delete=models.CASCADE)
    remoteurl = models.CharField(max_length=256, blank=False)  # rsync or cvs
    remotemodule = models.CharField(max_length=32, blank=False)
    lastsynced = models.DateTimeField(null=False, default=datetime.datetime.now)

    def __str__(self):
        return self.remoteurl

    class Meta:
        db_table = 'remoterepositories'
        verbose_name_plural = 'remote repositories'


class Repository(models.Model):
    repoid = models.AutoField(blank=False, primary_key=True)
    name = models.CharField(max_length=64, blank=False, unique=True)
    description = models.TextField(max_length=1024, blank=False)
    anonymous = models.BooleanField(blank=False, default=False, verbose_name='Enable anonymous access')
    web = models.BooleanField(blank=False, default=False, verbose_name='Enable gitweb access')
    approved = models.BooleanField(blank=False, default=False)
    tabwidth = models.IntegerField(default=8, null=False)
    initialclone = models.CharField(max_length=256, blank=True, null=True)
    remoterepository = models.ForeignKey(RemoteRepository, null=True, blank=True, on_delete=models.CASCADE,
                                         verbose_name='Remote repository')

    def ValidateOwnerPermissions(self, user):
        if not self.repositorypermission_set.filter(user=user, level=2).exists():
            raise Exception('You need owner permissions to do that!')

    def __str__(self):
        return self.name

    class Meta:
        db_table = 'repositories'
        verbose_name_plural = 'repositories'


class RepositoryPermission(models.Model):
    repository = models.ForeignKey(Repository, db_column='repository', on_delete=models.CASCADE)
    user = models.ForeignKey(User, null=False, blank=False, on_delete=models.CASCADE)
    level = models.IntegerField(default=0, verbose_name='Permission', choices=PERMISSION_CHOICES)

    @property
    def write(self):
        return (self.level > 0)

    @property
    def owner(self):
        return (self.level > 1)

    def __str__(self):
        return "%s (%s)" % (self.user.username, self.__permstr())

    def __permstr(self):
        if self.level == 2:
            return "Owner"
        elif self.level == 1:
            return "Write"
        return "Read"

    @property
    def username(self):
        return self.user.username

    class Meta:
        db_table = 'repository_permissions'
        unique_together = (('repository', 'user'), )


class GitUser(models.Model):
    user = models.OneToOneField(User, null=False, blank=False, primary_key=True, on_delete=models.CASCADE)
    sshkey = models.CharField(max_length=10240, blank=True)

    class Meta:
        db_table = 'git_users'
