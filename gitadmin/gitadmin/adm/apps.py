from django.apps import AppConfig


class GitAdmAppConfig(AppConfig):
    name = 'gitadmin.adm'

    def ready(self):
        from gitadmin.auth import auth_user_data_received, auth_user_created_from_upstream
        from gitadmin.adm.util import handle_user_data, handle_user_created

        auth_user_created_from_upstream.connect(handle_user_created)
        auth_user_data_received.connect(handle_user_data)
