# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import datetime


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='GitUser',
            fields=[
                ('userid', models.CharField(max_length=255, serialize=False, primary_key=True)),
                ('sshkey', models.CharField(max_length=10240, blank=True)),
            ],
            options={
                'db_table': 'git_users',
            },
        ),
        migrations.CreateModel(
            name='RemoteRepository',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('remoteurl', models.CharField(max_length=256)),
                ('remotemodule', models.CharField(max_length=32)),
                ('lastsynced', models.DateTimeField(default=datetime.datetime.now)),
            ],
            options={
                'db_table': 'remoterepositories',
                'verbose_name_plural': 'remote repositories',
            },
        ),
        migrations.CreateModel(
            name='RemoteRepositoryType',
            fields=[
                ('repotype', models.CharField(max_length=16, serialize=False, primary_key=True)),
            ],
            options={
                'db_table': 'remoterepositorytypes',
            },
        ),
        migrations.CreateModel(
            name='Repository',
            fields=[
                ('repoid', models.AutoField(serialize=False, primary_key=True)),
                ('name', models.CharField(unique=True, max_length=64)),
                ('description', models.TextField(max_length=1024)),
                ('anonymous', models.BooleanField(verbose_name='Enable anonymous access', default=False)),
                ('web', models.BooleanField(verbose_name='Enable gitweb access', default=False)),
                ('approved', models.BooleanField(default=False)),
                ('tabwidth', models.IntegerField(default=8)),
                ('initialclone', models.CharField(max_length=256, null=True, blank=True)),
                ('remoterepository', models.ForeignKey(verbose_name='Remote repository', blank=True, to='adm.RemoteRepository', null=True, on_delete=models.CASCADE)),
            ],
            options={
                'db_table': 'repositories',
                'verbose_name_plural': 'repositories',
            },
        ),
        migrations.CreateModel(
            name='RepositoryPermission',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('userid', models.CharField(max_length=255, verbose_name="User id")),
                ('level', models.IntegerField(default=0, verbose_name='Permission', choices=[(0, 'Read'), (1, 'Write'), (2, 'Owner')])),
                ('repository', models.ForeignKey(to='adm.Repository', db_column='repository', on_delete=models.CASCADE)),
            ],
            options={
                'db_table': 'repository_permissions',
            },
        ),
        migrations.AddField(
            model_name='remoterepository',
            name='repotype',
            field=models.ForeignKey(to='adm.RemoteRepositoryType', on_delete=models.CASCADE),
        ),
        migrations.AlterUniqueTogether(
            name='repositorypermission',
            unique_together=set([('repository', 'userid')]),
        ),
    ]
