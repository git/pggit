from django.contrib.auth.models import User
from django.db import connection

from gitadmin.auth import user_search, user_import, subscribe_to_user_changes

def get_or_import_user(email):
    try:
        return User.objects.get(email=email)
    except User.DoesNotExist:
        pass

    # If the user didn't exist, then try to import it. We have to start
    # by doing an email search, and then later import it.
    users = user_search(searchterm=email)
    if len(users) != 1 or users[0]['e'] != email:
        raise Exception("User not found")

    return user_import(users[0]['u'])


# New user created from upstream
def handle_user_created(sender, **kwargs):
    user = kwargs.pop('user')
    try:
        subscribe_to_user_changes(user.username)
    except Exception as e:
        print("Exception subscribing new user: %s", e)


# Updates arriving from community authentication
def handle_user_data(sender, **kwargs):
    user = kwargs.pop('user')
    userdata = kwargs.pop('userdata')

    if 'sshkeys' not in userdata:
        # ssh keys are not included on "live logins", only in the background feeds
        return

    curs = connection.cursor()
    if userdata.get('sshkeys', None):
        # We have an ssh key, so update the contents
        curs.execute("INSERT INTO git_users (user_id, sshkey) VALUES (%(uid)s, %(key)s) ON CONFLICT (user_id) DO UPDATE SET sshkey=excluded.sshkey", {
            'uid': user.id,
            'key': userdata['sshkeys'],
        })
    else:
        # No or empty ssh key upstream, so delete
        curs.execute("DELETE FROM git_users WHERE user_id=%(uid)s", {
            'uid': user.id,
        })
