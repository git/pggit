from django.core.exceptions import ValidationError
from django import forms
from django.forms import ModelForm, Form

from gitadmin.adm.models import Repository, RepositoryPermission, PERMISSION_CHOICES

from gitadmin.adm.util import get_or_import_user

class RepositoryForm(ModelForm):
    initialclone = forms.RegexField(r'^(git://.+/.+|[^:]+)$', max_length=256, required=False,
                                    label="Initial clone",
                                    help_text='Input a valid local repository name or git:// URL')

    class Meta:
        model = Repository
        exclude = ('repoid', 'name', 'remoterepository')


class ConfirmDeleteForm(Form):
    confirmed = forms.BooleanField(required=True, label="Confirm deleting the repository")


class RepositoryPermissionForm(forms.ModelForm):
    username = forms.CharField(label="User")
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['username'].widget = forms.widgets.TextInput(attrs={'readonly': 'readonly'})
        self.fields['username'].initial = "{} {} <{}>".format(self.instance.user.first_name, self.instance.user.last_name, self.instance.user.email)


class RepositoryPermissionsAddForm(forms.Form):
    addemail = forms.EmailField(label='Add email', required=False)
    addlevel = forms.ChoiceField(label='Permission', choices=PERMISSION_CHOICES)

    def __init__(self, repo, *args, **kwargs):
        self.repo = repo
        super().__init__(*args, **kwargs)

    def clean_addemail(self):
        e = self.cleaned_data['addemail']
        if not e:
            # Not mandatory!
            return e

        # Find the user?
        try:
            u = get_or_import_user(e)
        except Exception:
            raise ValidationError("User with this email address not found")

        if RepositoryPermission.objects.filter(user=u, repository=self.repo).exists():
            raise ValidationError("This user already has a permissions entry on this repository")

        return e
