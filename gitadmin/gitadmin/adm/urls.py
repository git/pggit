from django.urls import re_path

import gitadmin.adm.views
import gitadmin.auth

urlpatterns = [
    re_path(r'^$', gitadmin.adm.views.index),
    re_path(r'^repo/(\d+)/$', gitadmin.adm.views.editrepo),
    re_path(r'^repo/(\d+)/delete/$', gitadmin.adm.views.deleterepo),
    re_path(r'^new/$', gitadmin.adm.views.newrepo),
    re_path(r'^help/$', gitadmin.adm.views.help),

    # Log in/out
    re_path(r'^login/$', gitadmin.auth.login),
    re_path(r'^logout/$', gitadmin.auth.logout),
    re_path(r'^auth_receive/$', gitadmin.auth.auth_receive),
    re_path(r'^auth_api/$', gitadmin.auth.auth_api),
]
