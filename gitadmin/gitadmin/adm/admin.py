from django.contrib import admin
from gitadmin.adm.models import *


class RepositoryPermissionInline(admin.TabularInline):
    model = RepositoryPermission


class RepositoryAdmin(admin.ModelAdmin):
    list_display = ('name', 'approved', )
    ordering = ('approved', 'name', )
    inlines = [RepositoryPermissionInline, ]


admin.site.register(Repository, RepositoryAdmin)
admin.site.register(RemoteRepository)
