from django.urls import include, re_path

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

from gitadmin.feeds import AllReposFeed

# Feeds
feeds = {
    'all': AllReposFeed,
}


urlpatterns = [
    # Uncomment the next line to enable the admin:
    re_path(r'^adm/admin/', admin.site.urls),

    # Feeds
    re_path(r'^feeds/all/$', AllReposFeed()),

    re_path(r'adm/', include('gitadmin.adm.urls')),
]
